// react-testing-library renders your components to document.body,
// this adds jest-dom's custom assertions
import '@testing-library/jest-dom/extend-expect';
import '@testing-library/jest-dom';

import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';

import 'jest-styled-components';

// Init i18n for the tests needing it
import 'locales/i18n';

// src/setupTests.js
import { setupServer } from 'msw/node';
import { handlers } from './mocks';

// This configures a request mocking server with the given request handlers.
export const server = setupServer(...handlers);

// Start the server before any tests.
beforeAll(() => server.listen());

// Clean up after all tests are done, preventing memory leaks.
afterAll(() => server.close());
