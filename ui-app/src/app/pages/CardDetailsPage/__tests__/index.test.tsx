import React from 'react';
import {
  render,
  fireEvent,
  waitFor,
  getByPlaceholderText,
  getByRole,
} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'; // for additional matchers
import CardUI from '../CardUI';

describe('CardUI Component', () => {
  it('renders correctly', () => {
    const { getByPlaceholderText } = render(<CardUI />);
    const inputElement: any = getByPlaceholderText(
      'Enter your credit card number',
    );
    fireEvent.change(inputElement, { target: { value: '4242424242424242' } });
    expect(inputElement.value).toBe('4242424242424242');
  });

  it('validates card number correctly', async () => {
    const { getByPlaceholderText, getByText } = render(<CardUI />);
    const cardNumberInput = getByPlaceholderText(
      'Enter your credit card number',
    );
    const cardNameInput = getByPlaceholderText('Enter card holder name');
    const cardCvcInput = getByPlaceholderText('CVC');
    const cardDateInput = getByPlaceholderText('Expiration date');

    const validateButton = getByText('Validate');

    // Simulate entering an invalid card number
    fireEvent.change(cardNumberInput, { target: { value: 'haha' } });
    fireEvent.change(cardCvcInput, { target: { value: '123' } });
    fireEvent.change(cardDateInput, { target: { value: '2023-07' } });
    fireEvent.change(cardNameInput, { target: { value: 'Rahi' } });

    fireEvent.click(validateButton);

    // Check if validation message is displayed
    expect(
      getByText('The credit card number must be an integer.'),
    ).toBeInTheDocument();

    // Simulate entering a valid card number
    fireEvent.change(cardNumberInput, {
      target: { value: '4242424242424242' },
    });
    fireEvent.click(validateButton);

    // Wait for validation result
    await waitFor(() => {
      // Check if validation result is displayed
      expect(getByText('Valid: Yes')).toBeInTheDocument();
      expect(getByText('Card Type: Visa')).toBeInTheDocument();
    });
  });
});
