// src/mocks.js
import { rest } from 'msw';

export const handlers = [
  rest.post('http://localhost:3001/validate', (req: any, res, ctx) => {
    const { cardNumber } = req.body;
    if (cardNumber === '4242424242424242') {
      return res(
        ctx.json({
          isValid: true,
          cardType: 'Visa',
        }),
      );
    } else {
      return res(
        ctx.json({
          isValid: false,
          cardType: 'Unknown',
        }),
      );
    }
  }),
];
