const express = require('express');
const cors = require('cors');
const app = express();
const port = 3001;

app.use(cors()); // Use the cors middleware

// Middleware to parse JSON requests
app.use(express.json());

// Luhn algorithm for credit card validation
function luhnCheck(num) {
  const digits = num.toString().split('').map(Number);
  let sum = 0;
  for (let i = digits.length - 2; i >= 0; i -= 2) {
    let val = digits[i] * 2;
    if (val > 9) {
      val -= 9;
    }
    digits[i] = val;
  }
  for (let digit of digits) {
    sum += digit;
  }
  return sum % 10 === 0;
}

// Function to determine card type
function getCardType(cardNumber) {
  if (/^4/.test(cardNumber) && cardNumber.length === 16) {
    return 'Visa';
  }
  if (/^5[1-5]/.test(cardNumber) && cardNumber.length === 16) {
    return 'MasterCard';
  }
  if (/^3[47]/.test(cardNumber) && cardNumber.length === 15) {
    return 'American Express';
  }
  return 'Unknown';
}

// Endpoint to validate credit card numbers
app.post('/validate', (req, res) => {
  const { cardNumber } = req.body;
  if (!cardNumber || !/^\d+$/.test(cardNumber)) {
    return res.status(400).json({ error: 'Invalid card details' });
  }

  const isValid = luhnCheck(cardNumber);
  const cardType = getCardType(cardNumber);
  res.json({ isValid, cardType });
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
