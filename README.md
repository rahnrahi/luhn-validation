# Luhn Validation Project

A project that combines a React frontend (ui-app) with an Express backend (server) to create a web application for Luhn validation.

## Table of Contents

- [Overview](#overview)
- [Features](#features)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
  - [Starting the Development Servers](#starting-the-development-servers)
- [Usage](#usage)

## Overview

This project is a combination of a React frontend (ui-app) and an Express backend (server) to create a web application for Luhn validation. The React frontend provides a user interface built with TypeScript, while the Express backend handles server-side logic and APIs.

## Features

- React-based UI with TypeScript.
- Express server for backend logic.
- Development servers for both frontend and backend.
- Luhn validation feature.

## Getting Started

### Prerequisites

Before you begin, ensure you have met the following requirements:

- Node.js and npm installed.
- Basic understanding of React and Express.

### Installation

1. Clone the repository:

   ```sh
   git clone https://gitlab.com/rahnrahi/luhn-validation

```bash
cd luhn-validation

## Running the app
*We are split two folder: ui-app and server*

`Run only ui-app`

```bash
$ cd ui-app
$ npm i

# development

$ npm run start | yarn start

# build mode

$ npm run build | yarn build


```

`Run only server`

```bash

$ cd server
$ npm i
# development

$ npm run start



```



Usage
Access the React app by opening your web browser and navigating to http://localhost:3000.
The Express backend is accessible via its API endpoints at http://localhost:3001.

4012888888881881 (Visa)
5555555555554444 (Mastercard)
378282246310005 (American Express)